<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingsKeysToQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quizzes', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->after('id');
            $table->unsignedBigInteger('feedback_id')->after('id');

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('feedback_id')->references('id')->on('feedback');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quizzes', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
            $table->dropForeign(['feedback_id']);
            
            $table->dropColumn('category_id');
            $table->dropColumn('feedback_id');
        });
    }
}
