<?php 

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

trait ApiResponser
{
  private function successResponse($data, $code)
  {
    return response()->json($data, $code);
  }
  
  protected function errorResponse($msg, $code)
  {
    return response()->json(
      [
        'isError' => true, 
        'error' => $msg, 
        'code' => $code
      ],
      $code);
  }
  
  protected function showAll($collection, $code = 200)
  {
    return $this->successResponse(
      [
        'isError' => false, 
        'data' => $collection,
      ], 
      $code);
  }
  
  protected function showOne($instance, $code = 200)
  {
    return $this->successResponse(
      [
        'isError' => false, 
        'data' => $instance
      ], 
      $code);
  }
  
  protected function showSuccessMessage($msg, $code = 200)
  {
    return $this->successResponse(
      [
        'isError' => false, 
        'data' => $msg
      ], 
      $code);
  }
}
