<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    public function quizzes()
    {
       return $this->hasMany('App\Quiz');
    }
}
