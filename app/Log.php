<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [ 'quiz_id', 'user_id', 'dateShow' ];
    
    public function quiz()
    {
        return $this->belongsTo('App\Quiz', 'quiz_id');
    }
    
    public function user()
    {
       return $this->hasMany('App\User', 'user_id');
    }
}
