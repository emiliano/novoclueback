<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'firstName', 'secondName', 'initials', 'email', 'password',
        'avatar_id', 'avatar', 'job', 'gender', 'age', 'phone', 'status', 'email_verified_at', 'isAdmin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function wallets()
    {
       return $this->hasMany('App\Wallet');
    }

    public function log()
    {
        return $this->hasMany('App\Log');
    }
}
