<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'datePurchase', 'total', 'status', 'token', 'shopping_cart_id',
    ];

    public function shopping_carts()
    {
        return $this->belongsTo('App\ShoppingCart', 'shopping_cart_id');
    }

}
