<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    
    protected $fillable = [ 'question', 'value', 'correct_ans', 'image', 'mode', 'description', 'category_id', 'feedback_id' ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }
    
    public function feedback()
    {
        return $this->belongsTo('App\Feedback', 'feedback_id');
    }

    public function answers()
    {
       return $this->hasMany('App\Answer');
    }

    public function log()
    {
        return $this->hasMany('App\Log');
    }
}
