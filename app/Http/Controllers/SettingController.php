<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class SettingController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $setting = Setting::limit(1)->get();
        return $this->showOne($setting); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required|digits_between:1,20|numeric',
                'initialMessage' => 'required|max:300|string',
                'timeForResponse' => 'required|numeric',
                'multiplyPoints' => 'required|numeric'
            ]);
            DB::beginTransaction();
            $setting = Setting::find($request->get('id'));
            $setting->initialMessage = $request->get('initialMessage');
            $setting->timeforresponse = $request->get('timeForResponse');
            $setting->multiplypoints = $request->get('multiplyPoints');
            $setting->save();
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return $this->errorResponse('Algo falló', 200); 
        }
        return $this->showSuccessMessage('Configuración actualizada'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
