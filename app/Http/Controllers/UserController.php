<?php

namespace App\Http\Controllers;

use App\User;
use App\Wallet;
use App\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Mail;

class UserController extends ApiController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|string',
                'firstname' => 'required|string',
                'initials' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string'
            ]);
            DB::beginTransaction();
            $user = User::create([
                'name' => $request->name,
                'firstName' => $request->get('firstname'),
                'initials' => $request->get('initials'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'gender' => $request->get('gender'),
                'avatar' => 'artboard_1.svg',
                'age' => $request->get('age'),
                'phone' => $request->get('phone'),
                'isAdmin' => $request->get('isAdmin'),
            ]);
            if($user->id > 0){
                //Crear el wallet del usuario
                Wallet::create([
                'user_id' => $user->id
                ]);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error'         => true,
                'mensaje'        => $e,
            ], 500);
        }
        return response()->json( [
            'error'     => false
        ]
        , 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id > 0){
            $user = User::find($id);
        }else{
            $user = User::paginate();
        }
        return response()->json($user);
    }
    
     /**
     * Display the specified resource.
     *
     * @param  int  $limit
     * @return \Illuminate\Http\Response
     */
    public function showTop($top = 10)
    {
        $users = User::join('wallets', 'users.id', '=', 'wallets.user_id')->orderBy('wallets.points', 'DESC')->take($top)->get();
        
        return response()->json($users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->firstName = $request->get('firstName');
        $user->initials = $request->get('initials');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->avatar = $request->get('avatar');
        $user->job = $request->get('job');
        $user->gender = $request->get('gender');
        $user->age = $request->get('age');
        $user->phone = $request->get('phone');
        $user->status = $request->get('status');
        $user->save();

        return response()->json([
            'message' => 'Successfully updated user!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        
            return response()->json([
            'message' => 'Successfully deleted user!'
        ], 200);
    }
    
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showInfo($id){
        $user =  User::join('wallets', 'users.id', '=', 'wallets.user_id')
            ->where('users.id', $id)
            ->get();
        $log = Log::where('user_id', $id)->get();
        $quizCount = $log->count();   
        
        // $file_path = storage_path('app/avatars/avatar1.png');
        $path = 'avatar1.png';
        // $path = storage_path('public/' . 'avatar1.png');
        
        $data = [
            'id' => $user[0]->user_id,
            'name' => $user[0]->name,
            'firstName' => $user[0]->firstName,
            'email' => $user[0]->email,
            'avatar' => $user[0]->avatar,
            //'avatar_id' => $user[0]->avatar_id,
            'phone' => $user[0]->phone,
            'points' => $user[0]->points,
            'crown' => $user[0]->crown,
            'questionsAnswered' => $quizCount,
        ];
        return $this->showOne($data); 
    }
    
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateInfo(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->firstName = $request->get('firstName');
        $user->email = $request->get('email');
        $user->avatar = $request->get('avatar');
        $user->phone = $request->get('phone');
        $user->save();
        
        return $this->showSuccessMessage('Usuario actualizada'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function allusers(Request $request){
        $users = User::paginate(20);
        return response()->json( $users, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function userdetail($quiz)
    {
        $detail = User::where('id', $quiz)->get();
        return response()->json($detail);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|string',
                'firstName' => 'required|string',
                'initials' => 'required|string',
                'email' => 'required|string|email|unique:users,email,'.$request->get('id'),
                'password' => 'nullable|string'
            ]);
            DB::beginTransaction();
            $user = User::find($request->get('id'));
            $user->name = $request->get('name');
            $user->firstName = $request->get('firstName');
            $user->initials = $request->get('initials');
            $user->email = $request->get('email');
            $pas = $request->get('password');
            if(isset($pas)) {
            $user->password = bcrypt($request->get('password'));
                $subject = "Restablecimiento de contraseña";
                $for = ''.$request->get('email').'';
                $data = ['pass' => $pas];
                Mail::send('newpassword',$data, function($msj) use($subject,$for){
                    $msj->from("correo desde el que será enviado","nombre de la cuenta");
                    $msj->subject($subject);
                    $msj->to($for);
                });
            }
            $user->gender = $request->get('gender');
            $user->age = $request->get('age');
            $user->phone = $request->get('phone');
            $user->isAdmin = $request->get('isAdmin');
            $user->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error'         => true,
                'mensaje'        => $e,
            ], 500);
        }
        return response()->json( [
            'error'     => false
        ]
        , 201);
    }


}
