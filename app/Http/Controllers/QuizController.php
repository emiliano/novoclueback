<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Log;
use App\Answer;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\QuestionsImport;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class QuizController extends ApiController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'feedback_id' => 'required|digits_between:1,20|numeric',
            'category_id' => 'required|digits_between:1,20|numeric',
            'question' => 'required|max:300|string',
            'value' => 'required',
            'curious' => 'required|max:250|string',
            
            'mode' => 'required|max:10|string',
            'description' => 'required|max:50|string',
        ]);
        
        Quiz::create([
            'feedback_id' => $request->feedback_id,
            'category_id' => $request->category_id,
            'question' => $request->question,
            'value' => $request->value,
            'curious' => $request->curious,
            'image' => $request->image,
            'mode' => $request->mode,
            'description' => $request->description,
        ]);

        return $this->showSuccessMessage('Creado exitosamente', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function show($catId, $usrId)
    {
        // Find quizzes displayed
        $displayed = Log::select('quiz_id')->where('user_id', $usrId)->get();
        
        $quiz = Quiz::where('category_id', $catId)
            ->whereNotIn('id', $displayed)
            ->with('answers')
            ->inRandomOrder()
            ->limit(1)
            ->get();
        if($quiz->isEmpty()){
            return $this->errorResponse('Las preguntas de esta categoría se agotaron.', 200);
        }
        // Save In Log
        Log::create([
            'quiz_id' => $quiz[0]->id,
            'user_id' => $usrId,
            'dateShow' => Carbon::now()
        ]);
        
        return $this->showOne($quiz);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'feedback_id' => 'required|digits_between:1,20|numeric',
            'category_id' => 'required|digits_between:1,20|numeric',
            'question' => 'required|max:300|string',
            'value' => 'required',
            'curious' => 'required|max:250|string',
            
            'mode' => 'required|max:10|string',
            'description' => 'required|max:50|string',
        ]);
        
        $quiz = Quiz::find($id);
        $quiz->feedback_id = $request->get('feedback_id');
        $quiz->category_id = $request->get('category_id');
        $quiz->question = $request->get('question');
        $quiz->value = $request->get('value');
        $quiz->curious = $request->get('curious');
        $quiz->image = $request->get('image');
        $quiz->mode = $request->get('mode');
        $quiz->description = $request->get('description');
        $quiz->save();

        return $this->showSuccessMessage('Actualizado correctamente', 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quiz = Quiz::findOrFail($id);
        $quiz->delete();
        
        return $this->showSuccessMessage('Eliminado correctamente', 200);
    }
    
     /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request){
        $quizzes = Quiz::with('answers', 'category')->paginate(20);
        return $this->showAll($quizzes);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function quizdetail($quiz)
    {
        $detail = Quiz::with(
            array('answers'=>function($query){
                $query->select('id','quiz_id', 'content');
            }))->where('id', $quiz)
            ->select('id', 'category_id', 'question', 'correct_ans')
            ->get();
        return $this->showOne($detail);
        // return response()->json($detail);
    }
    
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateQuiz(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required|digits_between:1,20|numeric',
                'category_id' => 'required|digits_between:1,20|numeric',
                'question' => 'required|max:300|string',
                'answers' => 'required'
            ]);
            
            DB::beginTransaction();
            
            $quiz = Quiz::find($request->get('id'));
            
            $quiz->correct_ans = $request->get('correct_ans');
            $quiz->category_id = $request->get('category_id');
            $quiz->question = $request->get('question');
            $quiz->save();
            // $answers = Answer::where('quiz_id', $quiz->id);
            foreach($request->get('answers') as $a){
                $ans = Answer::find($a['id']);
                $ans->content = $a['content'];
                
                $ans->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'error'         => true,
                'mensaje'        => $e,
            ], 500);
        }
        return response()->json( [
            'error'     => false
        ]
        , 201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function quizzesupload(Request $request)
    {

        $file = request()->file('file');
        $result = array();
        $result = Excel::toArray(new QuestionsImport(), $file);

        foreach($result as $row => $value){
            foreach($value as $row)
            {
                $value2[] = array('no' => $row['no'],
                        'categoria' => $row['categoria'],
                        'pregunta' => $row['pregunta'],
                        'valor' => $row['valor'],
                        'respuesta_correcta' => $row['respuesta_correcta'],
                        'respuesta_a' => $row['respuesta_a'],
                        'respuesta_b' => $row['respuesta_b'],
                        'respuesta_c' => $row['respuesta_c'],
                        'respuesta_d' => $row['respuesta_d'],
                );
            }
        }

        $html='<div class="table-responsive">
            <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Categoria</th>
                <th scope="col">Pregunta</th>
                <th scope="col">Valor</th>
                <th scope="col">Respuesta Correcta</th>
                <th scope="col">Respuesta A</th>
                <th scope="col">Respuesta B</th>
                <th scope="col">Respuesta C</th>
                <th scope="col">Respuesta D</th>
                </tr>
            </thead>
            <tbody>';
            $category = 0;
            $question = 0;
            $val = 0;
            $correctanswer = 0;
            $answera = 0;
            $answerb = 0;
            $answerc = 0;
            $answerd = 0;
            $collection = collect($value2);
            $validator2 = Validator::make($value2, [
                '*.categoria' => 'required|'.Rule::in(['Fácil','Intermedio','Avanzado']),
                '*.pregunta' => 'required|max:300',
                '*.valor' => 'required|integer|digits_between:1,11',
                '*.respuesta_correcta' => 'required|max:10|string|'.Rule::in(['A','B','C','D']),
                '*.respuesta_a' => 'required|max:40',
                '*.respuesta_b' => 'required|max:40',
                '*.respuesta_c' => 'required|max:40',
                '*.respuesta_d' => 'required|max:40',
           ]);
            foreach ($value2 as $key => $value) {
                $valuesval[] = $value['pregunta'];
                $filtered = $collection->where('pregunta', $value['pregunta']);
                $filtered2 = $filtered->count();
                $checkques = Quiz::where('question',$value['pregunta'])->count();
                $validator = Validator::make($value, [
                    'categoria' => 'required|'.Rule::in(['Fácil','Intermedio','Avanzado']),
                    'pregunta' => 'required|max:300',
                    'valor' => 'required|integer|digits_between:1,11',
                    'respuesta_correcta' => 'required|max:10|string|'.Rule::in(['A','B','C','D']),
                    'respuesta_a' => 'required|max:40',
                    'respuesta_b' => 'required|max:40',
                    'respuesta_c' => 'required|max:40',
                    'respuesta_d' => 'required|max:40',
                ]);
                if ($validator->fails()) {
                    $html.='<tr bgcolor="#F8D7DA">';
                }else{
                    if ($checkques>='1' || $filtered2 >= '2') {
                        $html.='<tr bgcolor="#F8D7DA">';
                    }else{
                        $html.='<tr bgcolor="#D4EDDA">';
                    }
                }
                $html.='<th scope="row">'.$value['no'].'</th>
                <td><input type="text" class="form-control" value="'.$value['categoria'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['pregunta'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['valor'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['respuesta_correcta'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['respuesta_a'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['respuesta_b'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['respuesta_c'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['respuesta_d'].'" readonly></td>';
                if ($validator->fails()) {
                    $errors = $validator->errors();
                    $html.='<td>'.$errors->first().'</td>';
                }else{
                    if ($checkques>='1' || $filtered2 >= '2') {
                        $html.='<td><strong>Verificar la pregunta.</strong></td>';
                    }else{
                        $html.='<td><strong>Correcto</strong></td>';
                    }
                }
                $html.='</tr>';
                $array[] = $checkques;
            }
            $html.= '</tbody></table></div>';
            $collec = collect($array);
            $filte = $collec->contains(function ($value, $key) {
                return $value >= 1;
            });
            if ($validator2->fails()) {
                $errors = $validator2->errors();
                $html.='<div class="row"><div class="col-md-12" align="center"><div class="alert alert-danger" role="alert"><strong>Verifique la información de su archivo</strong></div></div></div>';
                $erro = true;
            }else{
                if ($filte == true || count($valuesval) > count(array_unique($valuesval))) {
                    $html.='<div class="row"><div class="col-md-12" align="center"><div class="alert alert-danger" role="alert"><strong>Verifique la información de su archivo.</strong></div></div></div>';
                    $erro = true;
                }else{
                    $erro = false;
                }
            }
            return response()->json([
                'error'         => $erro,
                'mensaje'        => $html,
            ], 201);
    }

    public function formquizzesupload(Request $request)
    {
        $file = request()->file('file');
        $result = array();
        $result = Excel::toArray(new QuestionsImport(), $file);
        foreach($result as $row => $value){
            foreach($value as $row)
            {
                $value2[] = array('no' => $row['no'],
                        'categoria' => $row['categoria'],
                        'pregunta' => $row['pregunta'],
                        'valor' => $row['valor'],
                        'respuesta_correcta' => $row['respuesta_correcta'],
                        'respuesta_a' => $row['respuesta_a'],
                        'respuesta_b' => $row['respuesta_b'],
                        'respuesta_c' => $row['respuesta_c'],
                        'respuesta_d' => $row['respuesta_d'],
                );
            }
        }

        try {
            foreach ($value2 as $key => $value) {
                switch ($value['categoria']) {
                    case 'Fácil':
                        $cat = '1';
                        break;
                    case 'Intermedio':
                        $cat = '2';
                        break;
                    case 'Avanzado':
                        $cat = '3';
                        break;
                }
                DB::beginTransaction();
                $question = Quiz::create([
                    'feedback_id' => '1',
                    'category_id' => $cat,
                    'question' => $value['pregunta'],
                    'value' => $value['valor'],
                    'correct_ans' => $value['respuesta_correcta'],
                    'image' => '',
                    'mode' => '',
                    'description' => ''
                ]);
                Answer::create([
                    'quiz_id' => $question->id,
                    'content' => $value['respuesta_a'],
                    'index' => 'A',
                    'isCorrect' => '0',
                ]);
                Answer::create([
                    'quiz_id' => $question->id,
                    'content' => $value['respuesta_b'],
                    'index' => 'B',
                    'isCorrect' => '0',
                ]);
                Answer::create([
                    'quiz_id' => $question->id,
                    'content' => $value['respuesta_c'],
                    'index' => 'C',
                    'isCorrect' => '0',
                ]);
                Answer::create([
                    'quiz_id' => $question->id,
                    'content' => $value['respuesta_d'],
                    'index' => 'D',
                    'isCorrect' => '0',
                ]);
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error'         => true,
                'mensaje'        => $e,
            ], 500);
        }
        return response()->json([
            'message' => 'Successfully created Quiz!'
        ], 201);
    }
}
