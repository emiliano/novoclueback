<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\ApiController;
use App\User;
use App\Wallet;
use Auth;
use Mail;

class AuthController extends ApiController
{
     /**
     * Registro de usuario
     */
    public function signUp(Request $request)
    {
        try{
            $request->validate([
                'name' => 'required|string',
                'firstName' => 'required|string',
                'initials' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string'
            ]);
            
            date_default_timezone_set("America/Mexico_City");
            $token=hash("md2",(string)microtime());

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'firstName' => $request->firstName,
                'initials' => $request->initials,
                'age' => $request->age,
                'phone' => $request->phone,
                'avatar' => 'artboard_1.svg',
                'gender' => $request->sex,
                'password' => bcrypt($request->password),
                'email_verified_at' => $token,
            ]);
            if($user->id > 0){
                //Crear el wallet del usuario
                Wallet::create([
                'user_id' => $user->id
                ]);
            }
            
            $subject = "Verificar correo electrónico";
            $for = ''.$request->email.'';
            $data = ['token' => $token, 'email' => $request->email];
            Mail::send('emailverification',$data, function($msj) use($subject,$for){
                $msj->from("novoclueserver@gmail.com","Novo Clue");
                $msj->subject($subject);
                $msj->to($for);
            });
        }catch(\Exception $e){
            // return $this->errorResponse($e->getMessage(), 200);
            return $this->errorResponse('Los datos son invalidos.', 200);
        }

        return $this->showSuccessMessage('Creado exitosamente', 201);
    }

    /**
     * Inicio de sesión y creación de token
     */
    public function login(Request $request)
    {
        try{
            $request->validate([
                'email' => 'required|string|email',
                'password' => 'required|string',
                'remember_me' => 'boolean'
            ]);
        }catch(\Exception $e){
            return $this->errorResponse($e->errors(), 200);
        }
        $userverify = User::where('email', $request->email)->limit(1)->get();
        if(count($userverify) == 0 || $userverify[0]->email_verified_at !== null){
            return $this->errorResponse('Tu usuario aun no esta verificado.', 200);
        }
        else{
            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials))
                return $this->errorResponse('Usuario o password incorrecto.', 200);
    
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
    
            $token = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
            
            $identified = [
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'user' => $user,
                'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
            ];
            
            return $this->showOne($identified);        
        }
    }
    
    public function verifyemail($email,$token)
    {
        $userverify = User::where([['email', $email],['email_verified_at',$token]])->count();
        
        if($userverify == '1'){
            User::where([['email', $email],['email_verified_at',$token]])->update(['email_verified_at' => null]);
            return redirect()->away('http://novoclue.com/#/login?usuario=activo');
        }else{
            return redirect()->away('http://novoclue.com/');
        }
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->successResponse('Log Out correcto.', 200);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
        return $this->showOne($request->user());
    }
}
