<?php

namespace App\Http\Controllers;

use DB;
use App\Log;
use App\User;
use App\Quiz;
use App\Purchase;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class LogController extends ApiController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Log  $log
   * @return \Illuminate\Http\Response
   */
  public function show(Log $log)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Log  $log
   * @return \Illuminate\Http\Response
   */
  public function edit(Log $log)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Log  $log
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Log $log)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Log  $log
   * @return \Illuminate\Http\Response
   */
  public function destroy(Log $log)
  {
      //
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * 
   * @return \Illuminate\Http\Response
   */
  public function generalInfo(Request $request)
  {
    $result = [];
    
    $usrlist = User::where('status', 1)->get();
    $result['userCount'] = $usrlist->count();
    $quizCount = Quiz::count();
    $result['quizCount'] = $quizCount;
    $purchaseCount = Purchase::count();
    $result['purchaseCount'] = $purchaseCount;
    $productCount = Product::count();
    $result['productCount'] = $productCount;
    
    return $this->showOne($result);
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * 
   * @return \Illuminate\Http\Response
   */
  public function getQuestionPerMonth(Request $request){
    $now = Carbon::now();
    
    $data = Log::select(DB::raw('count(*) as total'))
      ->whereYear('created_at', $now->year)
      ->groupBy(DB::raw("MONTH(created_at)"))
      ->get();  
      
      
    return $this->showOne($data); 
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * 
   * @return \Illuminate\Http\Response
   */
  public function getUsersMoreActives(Request $request){
    
    $data = Log::select(DB::raw('count(*) as total, users.id, users.name, users.firstname'))
      ->join('users', 'users.id', '=', 'logs.user_id')
      ->groupBy('logs.user_id')
      ->take(5)->get();
      
    return $this->showOne($data); 
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * 
   * @return \Illuminate\Http\Response
   */
  public function getLevelquestionPerMonth(Request $request){
    $now = Carbon::now();
    $months = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
    $levels = array(1, 2, 3);
    
    $results = [];
    foreach($months as $m){
      $row = [];
      foreach($levels as $l){
        $res = Log::select(DB::raw('count(logs.quiz_id) as ttl, categories.name'))
          ->join('quizzes', 'quizzes.id', '=', 'logs.quiz_id')
          ->join('categories', 'categories.id', '=', 'quizzes.category_id')
          ->where('categories.id', $l)
          ->whereMonth('logs.created_at', $m)
          ->whereYear('logs.created_at', $now->year)
          ->groupBy('quizzes.category_id')
          ->get();
          $row[] =  $res;
      }
      array_push ( $results , $row );
     
    }
    
    return $this->showOne( $results ); 
  }
}
