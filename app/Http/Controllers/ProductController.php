<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductsImport;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|max:100|string',
                'description' => 'nullable|max:300|string',
                'image' => 'required|mimes:jpg,jpeg,png|max:2048|dimensions:width=620,height=700',
                'sku' => 'nullable',
                'price' => 'required'
            ]);
            DB::beginTransaction();
            Product::create([
                'name' => $request->name,
                'description' => $request->description,
                'image' => $request->image->store('products'),
                'sku' => $request->sku,
                'price' => $request->price
            ]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error'         => true,
                'mensaje'        => $e,
            ], 500);
        }
        return response()->json( [
            'error'     => false
        ]
        , 201);
        return response()->json([
            'message' => 'Successfully created product!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0)
    {
        if($id > 0){
            $product = Product::where('id',$id)->get();
            if($product->isEmpty()){
                return $this->errorResponse('El producto no se encuentra disponible.', 200);
            }
            return $this->showOne($product);
        }else{
            $product = Product::all();
            if($product->isEmpty()){
                return $this->errorResponse('La tienda no tiene productos para mostrar.', 200);
            }
            return $this->showAll($product);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:100|string',
            'description' => 'nullable|max:300|string',
            'image' => 'nullable|mimes:jpg,jpeg,png|max:2048|dimensions:width=620,height=700',
            'sku' => 'nullable',
            'price' => 'required'
        ]);
        
        $product = Product::find($request->get('id'));
        $product->name = $request->get('name');
        $product->description = $request->get('description');
        if(isset($request->image)){
            $product->image = $request->image->store('products');
        }
        $product->sku = $request->get('sku');
        $product->price = $request->get('price');
        $product->save();

        return response()->json([
            'message' => 'Successfully updated product!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        
            return response()->json([
            'message' => 'Successfully deleted product!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request){
        $products = Product::paginate(20);
        return response()->json( $products, 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function productdetail($quiz)
    {
        $detail = Product::where('id', $quiz)->get();
        return response()->json($detail);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProduct(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required|digits_between:1,20|numeric',
                'name' => 'required|max:100|string',
                'description' => 'nullable|max:300|string',
                'image' => 'nullable|mimes:jpg,jpeg,png|max:2048|dimensions:width=620,height=700',
                'sku' => 'nullable',
                'price' => 'required'
            ]);
            DB::beginTransaction();
            $product = Product::find($request->get('id'));
            $product->name = $request->get('name');
            $product->description = $request->get('description');
            if(isset($request->image)){
                $product->image = $request->image->store('products');
            }
            $product->sku = $request->get('sku');
            $product->price = $request->get('price');
            $product->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error'         => true,
                'mensaje'        => $e,
            ], 500);
        }
        return response()->json( [
            'error'     => false
        ]
        , 201);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productupload(Request $request)
    {

        $file = request()->file('file');
        $result = array();
        $result = Excel::toArray(new ProductsImport(), $file);
        foreach($result as $row => $value){
            foreach($value as $row)
            {
                $value2[] = array('no' => $row['no'],
                        'nombre' => $row['nombre'],
                        'descripcion' => @$row['descripcion'],
                        'sku' => $row['sku'],
                        'precio' => $row['precio'],
                );
            }
        }
        $html='<div class="table-responsive">
            <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripción</th>
                <th scope="col">Sku</th>
                <th scope="col">Precio</th>
                </tr>
            </thead>
            <tbody>';
            $collection = collect($value2);
            $validator2 = Validator::make($value2, [
                '*.nombre' => 'required|max:100|string',
                '*.descripcion' => 'nullable|max:300',
                '*.sku' => 'nullable',
                '*.precio' => 'required',
           ]);
            foreach ($value2 as $key => $value) {
                $valuesval[] = $value['nombre'];
                $filtered = $collection->where('nombre', $value['nombre']);
                $filtered2 = $filtered->count();
                $checkques = Product::where('name',$value['nombre'])->count();
                $validator = Validator::make($value, [
                    'nombre' => 'required|max:100|string',
                    'descripcion' => 'nullable|max:300|string',
                    'sku' => 'nullable',
                    'precio' => 'required',
                ]);
                if ($validator->fails()) {
                    $html.='<tr bgcolor="#F8D7DA">';
                }else{
                    if ($checkques>='1' || $filtered2 >= '2') {
                        $html.='<tr bgcolor="#F8D7DA">';
                    }else{
                        $html.='<tr bgcolor="#D4EDDA">';
                    }
                }
                $html.='<th scope="row">'.$value['no'].'</th>
                <td><input type="text" class="form-control" value="'.$value['nombre'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['descripcion'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['sku'].'" readonly></td>
                <td><input type="text" class="form-control" value="'.$value['precio'].'" readonly></td>';
                if ($validator->fails()) {
                    $errors = $validator->errors();
                    $html.='<td>'.$errors->first().'</td>';
                }else{
                    if ($checkques>='1' || $filtered2 >= '2') {
                        $html.='<td><strong>Verificar el producto.</strong></td>';
                    }else{
                        $html.='<td><strong>Correcto</strong></td>';
                    }
                }
                $html.='</tr>';
                $array[] = $checkques;
            }
            $html.= '</tbody></table></div>';
            $collec = collect($array);
            $filte = $collec->contains(function ($value, $key) {
                return $value >= 1;
            });
            if ($validator2->fails()) {
                $errors = $validator2->errors();
                $html.='<div class="row"><div class="col-md-12" align="center"><div class="alert alert-danger" role="alert"><strong>Verifique la información de su archivo</strong></div></div></div>';
                $erro = true;
            }else{
                if ($filte == true || count($valuesval) > count(array_unique($valuesval))) {
                    $html.='<div class="row"><div class="col-md-12" align="center"><div class="alert alert-danger" role="alert"><strong>Verifique la información de su archivo.</strong></div></div></div>';
                    $erro = true;
                }else{
                    $erro = false;
                }
            }
            return response()->json([
                'error'         => $erro,
                'mensaje'        => $html,
            ], 201);
    }

    public function formproductupload(Request $request)
    {
        $file = request()->file('file');
        $result = array();
        $result = Excel::toArray(new ProductsImport(), $file);
        foreach($result as $row => $value){
            foreach($value as $row)
            {
                $value2[] = array('no' => $row['no'],
                        'nombre' => $row['nombre'],
                        'descripcion' => $row['descripcion'],
                        'sku' => $row['sku'],
                        'precio' => $row['precio'],
                );
            }
        }

        try {
            foreach ($value2 as $key => $value) {
                DB::beginTransaction();
                $question = Product::create([
                    'name' => $value['nombre'],
                    'description' => $value['descripcion'],
                    'sku' => $value['sku'],
                    'price' => $value['precio'],
                ]);
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error'         => true,
                'mensaje'        => $e,
            ], 500);
        }
        return response()->json([
            'message' => 'Successfully created Quiz!'
        ], 201);
    }


}
