<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:40|string',
            'description' => 'required|max:400|string'
        ]);
        
        Feedback::create([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return response()->json([
            'message' => 'Successfully created feedback!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0)
    {
        if($id > 0){
            $feedback = Feedback::findOrFail($id);
        }else{
            $feedback = Feedback::paginate();
        }
        return response()->json($feedback);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'name' => 'required|max:40|string',
                'description' => 'required|max:400|string'
            ]);
            
            $feedback = Feedback::findOrFail($id);
            $feedback->name           = $request->get('name');
            $feedback->description           = $request->get('description');
            $feedback->save();
        } catch (\Throwable $th) {
            return $this->errorResponse($th->errorInfo, 400);
        }
        return response()->json([
            'message' => 'Successfully updated feedback!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $feedback = Feedback::findOrFail($id);
            $feedback->delete();
        } catch (\Throwable $th) {
            return $this->errorResponse($th->errorInfo, 404);
        }
        return response()->json([
            'message' => 'Successfully deleted feedback!'
        ], 200);
    }
}
