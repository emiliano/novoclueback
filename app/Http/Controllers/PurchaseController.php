<?php

namespace App\Http\Controllers;

use App\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$request->validate([
            'datePurchase' => 'required',
            'total' => 'required',
        ]);*/
        
        Purchase::create([
            'shopping_cart_id' => $request->shopping_cart_id,
            'datePurchase' => $request->datePurchase,
            'total' => $request->total,
        ]);

        return response()->json([
            'message' => 'Successfully created purchase!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0)
    {
        if($id > 0){
            $purchase = Purchase::find($id);
        }else{
            $purchase = Purchase::paginate();
        }
        return response()->json($purchase);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$request->validate([
            'datePurchase' => 'required',
            'total' => 'required',
        ]);*/
        
        $purchase = Purchase::find($id);
        $purchase->datePurchase = $request->get('datePurchase');
        $purchase->total = $request->get('total');
        $purchase->save();

        return response()->json([
            'message' => 'Successfully updated purchase!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purchase = Purchase::findOrFail($id);
        $purchase->delete();
        
            return response()->json([
            'message' => 'Successfully deleted purchase!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function alltokens(Request $request){
        $purchases = Purchase::with('shopping_carts.product')->whereNotNull('token')->paginate(20);
        return response()->json( $purchases, 200);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updatetoken(Request $request)
    {
        try {
            DB::beginTransaction();
            Purchase::where([['id',$request->get('token')],['token',$request->get('info')]])->update(['token' => null, 'status' => 'Entregado']);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error'         => true,
                'mensaje'        => $e,
            ], 500);
        }
        return response()->json( [
            'error'     => false
        ]
        , 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function alltransactions(Request $request){
        $purchases = Purchase::with('shopping_carts.product')->paginate(20);
        return response()->json( $purchases, 200);
    }

}
