<?php

namespace App\Http\Controllers;

use App\ShoppingCart;
use App\Purchase;
use App\Wallet;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Mail;
use App\Http\Controllers\ApiController;

class ShoppingCartController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$request->validate([
            'quantity' => 'required',
            'amount' => 'required',
        ]);*/
        try{
            $shopping = ShoppingCart::create([
                'user_id' => $request->user_id,
                'product_id' => $request->product_id,
                'quantity' => $request->quantity,
                'amount' => $request->amount,
                'status' => $request->status,
            ]);
            date_default_timezone_set("America/Mexico_City");
            $token=hash("md2",(string)microtime());
            Purchase::create([
                'shopping_cart_id' => $shopping->id,
                'datePurchase' => date("Y-m-d H:i:s"),
                'total' => $request->amount,
                'status' => "Pendiente",
                'token' => $token,
            ]);
            $wallet = Wallet::where('user_id', $request->user_id)->limit(1)->get();
            $residue = $wallet[0]->crown - $request->amount;
            Wallet::where('user_id', $request->user_id)->update(['crown' => $residue]);
            $data = Product::where('id', $request->product_id)->limit(1)->get();
            $user = User::where('id', $request->user_id)->limit(1)->get();
            $subject = "Tu compra está en camino";
            $for = ''.$user[0]->email.'';
            $product = ['product' => $data[0]->name, 'token' => $token];
            Mail::send('email',$product, function($msj) use($subject,$for){
                $msj->from("correo desde el que será enviado","nombre de la cuenta");
                $msj->subject($subject);
                $msj->to($for);
            });
        }catch(\Exception $e){
            return $this->errorResponse('Tu compra no ha sido completada.', 200);
        }

        return $this->showSuccessMessage('Tu compra ha sido completada con éxito', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id > 0){
            $shopping = ShoppingCart::find($id);
        }else{
            $shopping = ShoppingCart::paginate();
        }
        return response()->json($shopping);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function edit(ShoppingCart $shoppingCart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /*$request->validate([
            'quantity' => 'required',
            'amount' => 'required',
        ]);*/

        $shopping = ShoppingCart::find($id);
        $shopping->quantity = $request->get('quantity');
        $shopping->amount = $request->get('amount');
        $shopping->status = $request->get('status');
        $shopping->save();

        return response()->json([
            'message' => 'Successfully updated shopping cart!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shopping = ShoppingCart::findOrFail($id);
        $shopping->delete();
        
            return response()->json([
            'message' => 'Successfully deleted shopping cart!'
        ], 200);
    }
}
