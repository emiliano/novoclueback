<?php

namespace App\Http\Controllers;

use App\Wallet;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|digits_between:1,20|numeric',
            'points' => 'required|digits_between:1,11|numeric',
            'crown' => 'required',
            'description' => 'nullable|max:400|string',
        ]);
        
        Wallet::create([
            'user_id' => $request->user_id,
            'points' => $request->points,
            'crown' => $request->crown,
            'description' => $request->description,
        ]);

        return response()->json([
            'message' => 'Successfully created wallet!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0)
    {
        // if($id > 0){
        //     $wallet = Wallet::find($id);
        // }else{
        //     $wallet = Wallet::paginate();
        // }
        $quiz = Wallet::where('user_id', $id)->get();
        return response()->json($quiz);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            // 'user_id' => 'required|digits_between:1,20|numeric',
            // 'points' => 'required|digits_between:1,11|numeric',
            'crown' => 'required',
            // 'description' => 'nullable|max:400|string',
        ]);

        $wallet = Wallet::find($id);
        $wallet->points = $request->get('points');
        $wallet->crown = $request->get('crown');
        // $wallet->description = $request->get('description');
        $wallet->save();

        return response()->json($wallet, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wallet = Wallet::findOrFail($id);
        $wallet->delete();
        
        return response()->json([
            'message' => 'Successfully deleted wallet!'
        ], 200);
    }
}
