<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Quiz;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'quiz_id' => 'required|digits_between:1,20|numeric',
            'content' => 'required|max:40|string',
            'isCorrect' => 'required',
        ]);
        
        Answer::create([
            'quiz_id' => $request->quiz_id,
            'content' => $request->content,
            'isCorrect' => $request->isCorrect,
        ]);

        return response()->json([
            'message' => 'Successfully created answer!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id > 0){
            $answer = Answer::find($id);
        }else{
            $answer = Answer::paginate();
        }
        return response()->json($answer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Answer $answer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'quiz_id' => 'required|digits_between:1,20|numeric',
            'content' => 'required|max:40|string',
            'isCorrect' => 'required',
        ]);
        
        $answer = Answer::find($id);
        $answer->quiz_id = $request->get('quiz_id');
        $answer->content = $request->get('content');
        $answer->isCorrect = $request->get('isCorrect');
        $answer->save();

        return response()->json([
            'message' => 'Successfully updated answer!'
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answer = Answer::findOrFail($id);
        $answer->delete();
        
        return response()->json([
            'message' => 'Successfully deleted answer!'
        ], 200);
    }
    
    // public function dummy(Request $request)
    // {
    //     $quizes = Quiz::all();
    //     $indexes = ['A', 'B', 'C', 'D'];
    //     foreach($quizes as $q){
    //         $answers = Answer::where('quiz_id', $q->id)->get();
    //         $i = 0;
    //         foreach($answers as $a){
    //             $ans = Answer::find($a->id);
    //             $ans->index = $indexes[$i];
    //             $ans->save();
    //             $i++;
    //         }
    //     }

    //     return response()->json([
    //         'message' => $quizes
    //     ], 201);
    // }
}
