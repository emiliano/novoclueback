<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1', 'middleware' => 'cors'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signUp');

    Route::get('top/{top?}', 'UserController@showTop');
    Route::group([
      'middleware' => 'auth:api'
    ], 
    function() {
      Route::get('logout', 'AuthController@logout');
      Route::get('user', 'AuthController@user');
      
      //Rutas aquí
      Route::get('feedback/{id?}', 'FeedbackController@show');
      Route::post('feedback', 'FeedbackController@store');
      Route::put('feedback/{id}', 'FeedbackController@update');
      Route::delete('feedback/{id}', 'FeedbackController@destroy');

      //Categories
      Route::get('category/{id?}', 'CategoryController@show');
      Route::post('category', 'CategoryController@store');
      Route::put('category/{id}', 'CategoryController@update');
      Route::delete('category/{id}', 'CategoryController@destroy');

      //Products
      Route::get('product/{id?}', 'ProductController@show');
      Route::post('product', 'ProductController@store');
      Route::put('product/{id}', 'ProductController@update');
      Route::delete('product/{id}', 'ProductController@destroy');

      //Wallets
      Route::get('wallet/{id?}', 'WalletController@show');
      Route::post('wallet', 'WalletController@store');
      Route::put('wallet/{id}', 'WalletController@update');
      Route::delete('wallet/{id}', 'WalletController@destroy');

      //Quiz
      Route::get('quiz/{catId}/{usrId}', 'QuizController@show');
      Route::post('quiz', 'QuizController@store');
      Route::put('quiz/{id}', 'QuizController@update');
      Route::delete('quiz/{id}', 'QuizController@destroy');

      //Answer
      Route::get('answer/{id?}', 'AnswerController@show');
      Route::post('answer', 'AnswerController@store');
      Route::put('answer/{id}', 'AnswerController@update');
      Route::delete('answer/{id}', 'AnswerController@destroy');
      // Route::post('answer/dummy', 'AnswerController@dummy');

      //Purchase
      Route::get('purchase/{id?}', 'PurchaseController@show');
      Route::post('purchase', 'PurchaseController@store');
      Route::put('purchase/{id}', 'PurchaseController@update');
      Route::delete('purchase/{id}', 'PurchaseController@destroy');

      //Purchase
      Route::get('shoppingcart/{id?}', 'ShoppingCartController@show');
      Route::post('shoppingcart', 'ShoppingCartController@store');
      Route::put('shoppingcart/{id}', 'ShoppingCartController@update');
      Route::delete('shoppingcart/{id}', 'ShoppingCartController@destroy');

      //User
      Route::get('users/{id?}', 'UserController@show');
      Route::post('users', 'UserController@store');
      Route::put('users/{id}', 'UserController@update');
      Route::delete('users/{id}', 'UserController@destroy');
      
      //Settings
      Route::get('userinfo/{id}', 'UserController@showInfo');
      Route::put('userinfo/{id}', 'UserController@updateInfo');
    });
    Route::group([
      'prefix' => 'admin', 'middleware' => 'auth:api'
    ], function(){
      //Quiz
      Route::get('allquiz', 'QuizController@all');
      Route::get('quizdetail/{quiz}', 'QuizController@quizdetail');
      Route::put('quizdetail', 'QuizController@updateQuiz');
      Route::post('quizzesupload', 'QuizController@quizzesupload');
      Route::post('formquizzesupload', 'QuizController@formquizzesupload');
      
      //Categories
      Route::get('category/{id?}', 'CategoryController@show');
      
      //Settings
      Route::get('settings', 'SettingController@show');
      Route::put('settings', 'SettingController@update');

      //Products
      Route::post('product', 'ProductController@store');
      Route::get('allproducts', 'ProductController@all');
      Route::get('productdetail/{product}', 'ProductController@productdetail');
      Route::post('productdetail', 'ProductController@updateProduct');
      Route::delete('productdelete/{id}', 'ProductController@destroy');
      Route::post('productupload', 'ProductController@productupload');
      Route::post('formproductupload', 'ProductController@formproductupload');

      //Tokens
      Route::get('alltokens', 'PurchaseController@alltokens');
      Route::post('tokendetail', 'PurchaseController@updatetoken');

      //Transactions
      Route::get('alltransactions', 'PurchaseController@alltransactions');

      //Users
      Route::post('user', 'UserController@store');
      Route::get('allusers', 'UserController@allusers');
      Route::get('userdetail/{user}', 'UserController@userdetail');
      Route::put('userdetail', 'UserController@updateUser');
      Route::delete('userdelete/{id}', 'UserController@destroy');
      
      //General Info
      Route::get('generalInfo', 'LogController@generalInfo');
      Route::get('getQuestionPerMonth', 'LogController@getQuestionPerMonth');
      Route::get('getUsersMoreActives', 'LogController@getUsersMoreActives');
      Route::get('getLevelsPerMonth', 'LogController@getLevelquestionPerMonth');
    });
});
